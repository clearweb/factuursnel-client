<?php namespace Factuursnel\Client;

use \Clearweb\Clearworks\Action\ActionAnchor;

use Factuursnel\CustomPrice\Page\OverviewPage;

class CustomPriceLink extends ActionAnchor
{
	function init() {
		$this->setTitle(ucfirst(trans_choice('app.customprice', 2)));
		
		return parent::init();
	}
	
	function execute() {
		$this->setUrl(\Clearworks::getPageUrl(new OverviewPage, array('client_id'=>$this->getParameter('id'))));
		
		return parent::execute();
	}
}
