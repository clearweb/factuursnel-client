<?php namespace Factuursnel\Client;

use Clearweb\Clearwebapps\Eloquent\ModelFieldScaffolder;

use Factuursnel\Address\Address;

class FormWidget extends \Clearweb\Clearwebapps\Eloquent\FormWidget
{
	function init() {
		parent::init();
		
		$address = $this->getModel()->address;
		if (empty($address)) {
			$address = new Address;
		}
		
		$this->getForm()->addFields(
									ModelFieldScaffolder::getModelFields($address, $this->hidden_fields),
									3 //hidden fields are counted as field too.
									)
			->removeField('address_id')
            ->moveField('phone', 3)
            ->moveField('email', 4)
			;
		
		return $this;
	}
	
	
	public function execute() {
		parent::execute();
		$address = $this->getModel()->address()->first();
		if ( ! empty($address)) {
			$this->getForm()->setValues($address->toArray());
		}
	}
	
	public function submit(array $post) {
		$address_keys = array(
							 'street',
							 'number',
							 'extension',
							 'postal_code',
							 'city',
							 'country',
							 );
		
		$address = $this->getModel()->address;
		if (empty($address)) {
			$address = new Address;
		}
		
		foreach(array_only($post, $address_keys) as $key => $value) {
			$address->$key = $value;
		}
		
		$address->save();
		
		$post['address_id'] = $address->id;
		return parent::submit(array_except($post, $address_keys));
	}
}