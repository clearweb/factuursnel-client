<?php namespace Factuursnel\Client;

class Client extends \Eloquent
{
	protected $table = 'client';
	protected $fillable = array('name', 'first_name', 'name_infix', 'last_name', 'phone', 'email', 'address_id');

    public function getNumber()
    {
        return 'CL'.str_pad($this->id, 5, '0', STR_PAD_LEFT);
    }
    
	function address() {
		return $this->belongsTo('\Factuursnel\Address\Address');
	}
}