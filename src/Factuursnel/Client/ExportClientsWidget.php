<?php namespace Factuursnel\Client;

use Factuursnel\Client\Client;

use Clearweb\Clearwebapps\Widget\FormWidget;

use Clearweb\Clearwebapps\Form\Form;

use Clearweb\Clearwebapps\Form\DateField;
use Clearweb\Clearwebapps\Form\SubmitField;

use Clearweb\Clearwebapps\Form\Validator;

use Clearweb\Clearwebapps\File\File;
use FileManager;

use Clearweb\Clearworks\Action\ActionAnchor;

use Factuursnel\Base\Vat\Vat;

class ExportClientsWidget extends FormWidget
{
    private $csvFile = null;
    
    public function init()
    {
        $this->setName('export-clients')
             ->setForm(
                 (new Form)
                 ->addField(
                     (new DateField)
                     ->setName('date_from')
                     ->setLabel(trans('client::export.date_from'))
                 )
                 ->addField(
                     (new DateField)
                     ->setName('date_till')
                     ->setLabel(trans('client::export.date_till'))
                 )
                 ->addField(
                     (new SubmitField)
                     ->setName('export')
                     ->setLabel(trans('client::export.export'))
                 )
             )
             ->setValidator((new Validator)->setRules([
                 'date_from' => 'date',
                 'date_till' => 'date',
             ]))
             ->setTitle(trans('client::export.export'))
            ;
        return parent::init();
    }

    public function execute()
    {

        
        if ($this->getSubmitted()) {
            $file = $this->getCSVFile();
            
        }

        parent::execute();
        
        return $this;
    }

    public function submit(array $post)
    {
        $dateFrom = empty($post['date_from']) ? null : $post['date_from'];
        $dateTill = empty($post['date_till']) ? null : $post['date_till'];
        
        $file = $this->writeExportFile($dateFrom, $dateTill);

        $this->setCSVFile($file);

        $link = (new ActionAnchor)->setURL($file->getUrl())->setTitle(trans('client::export.download_export'))->addClass('button')->addClass('btn');
        $this->addViewable($link);
        //$this->addAction('download', $link);
    }

    function writeExportFile($dateFrom, $dateTill)
    {
        $fileName = "client";

        if ($dateFrom && $dateTill) {
            $fileName .= "_{$dateFrom}_{$dateTill}";
        }
        
        $fileName .= '.csv';
        
        $file = FileManager::generateNewFile($fileName);
        
        if ($dateFrom && $dateTill) {
            $clients = Client::whereBetween('created_at', [$dateFrom . ' 00:00:00', $dateTill . ' 23:59:59'])->get();
        } else {
            $clients = Client::all();
        }
        
        $this->writeCSV($file, $clients);
        
        return $file;
    }

    public function writeCSV(File $file, $clients)
    {
        $csvFile  = fopen($file->getFullPath(), 'w');
        $vatTypes = Vat::all();
        
        $line = [
                 'id',
                 'number',
                 'name',
                 'email',
                 'phone',
                 'street',
                 'house-number',
                 'extension',
                 'postcode',
                 'city',
                 'date'
                 ];
        
        fputcsv($csvFile, $line);
        
        foreach($clients as $client) {
            $line = [
                     $client->id,
                     $client->getNumber(),
                     $client->name,
                     $client->email,
                     $client->phone,
                     $client->address->street,
                     $client->address->house_number,
                     $client->address->extension,
                     $client->address->postal_code,
                     $client->address->city,
                     date('Y-m-d', strtotime($client->created_at)),
                     ];
            
            fputcsv($csvFile, $line);
        }
        
        fclose($csvFile);
    }

    public function setCSVFile(File $file)
    {
        $this->csvFile = $file;
        
        return $this;
    }

    public function getCSVFile()
    {
        return $this->csvFile;
    }
}