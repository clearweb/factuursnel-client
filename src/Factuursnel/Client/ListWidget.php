<?php namespace Factuursnel\Client;

use Clearweb\Clearwebapps\Eloquent\SortableListWidget;

class ListWidget extends SortableListWidget
{
	public function init() {
        $this->setModelClass('\Factuursnel\Client\Client');
		parent::init();
        
        $this->addStyle('invoices/css/invoice-list.css')
            ->setOrderAttribute('created_at')
            ->setOrderDirection('desc')
            ;
        
		$this
			->setShowNewButton(true)
			->getList()
			->removeColumn('address')
			->addActionLink(new DeleteActionLink, 'delete')
			;
        
        return $this;
	}
    
    public function execute()
    {
        parent::execute();
        
        $this->getContainer()->addClass('client-list');
        
        return $this;
    }
}
