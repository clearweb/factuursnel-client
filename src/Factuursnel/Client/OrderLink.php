<?php namespace Factuursnel\Client;

use \Clearweb\Clearworks\Action\ActionAnchor;
use \Order\Page\OverviewPage as OrderOverview;

class OrderLink extends ActionAnchor
{
	function init() {
		$this->setTitle(ucfirst(trans_choice('app.order', 2)));
		
		return parent::init();
	}
	
	function execute() {
		$this->setUrl(\Clearworks::getPageUrl(new OrderOverview, array('client_id'=>$this->getParameter('id'))));
		
		return parent::execute();
	}
}