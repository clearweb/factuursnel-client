<?php namespace Factuursnel\Client;

use Clearweb\Clearworks\Action\ActionAnchor;

use Factuursnel\Invoice\Page\OverviewPage;

class InvoicesLink extends ActionAnchor
{
	function init() {
		$this->setTitle(ucfirst(trans_choice('app.invoice', 2)));
		
		return parent::init();
	}
	
	function execute() {
		$this->setUrl(\Clearworks::getPageUrl(new OverviewPage, array('client_id'=>$this->getParameter('id'))));
		
		return parent::execute();
	}
}