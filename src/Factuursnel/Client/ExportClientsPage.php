<?php namespace Factuursnel\Client;

use Clearweb\Clearwebapps\Layout\FullWidthLayout;

use Factuursnel\Client\Client;

use Clearweb\Clearwebapps\Page\Page as BasePage;

class ExportClientsPage extends BasePage
{
    public function __construct() {
        $this->setSlug(trans('client::nav.export_clients'));
    }

    public function init() {
		$this->setLayout(new FullWidthLayout);
        
        $client = Client::find($this->getParameter('client_id', 0));
        
        
        if (empty($client)) {
			$this->setTitle(ucfirst(trans('client::export.export_clients')));
		} else {
            $this->setTitle(ucfirst(trans('client::export.export_clients_for', array('client'=>$client->name))));
        }

                    
        $this->addWidgetLinear(new ExportClientsWidget, 'content');
        
        return parent::init();
    }
}