<?php namespace Factuursnel\Client;

use Clearweb\Clearwebapps\Layout\OverviewLayout;

use Clearweb\Clearworks\Action\ActionAnchor;

use Clearweb\Clearwebapps\Page\Page as BasePage;

use Clearworks;

class Page extends BasePage
{
	public function __construct() {
		$this->setSlug(trans('nav.clients'));
		$this->setTitle(ucfirst(trans_choice('app.client', 2)));
	}
	
	public function init() {
		$this->setLayout(new OverviewLayout);
		$this->addWidgetLinear(new FormWidget, 'right', 1)
			->addWidgetLinear(new ListWidget, 'left', 2)
			;

        $this->addAction(
                         'export-clients',
                         with(new ActionAnchor)
                         ->setUrl(Clearworks::getPageUrl(new ExportClientsPage))
                         ->setTitle(trans('client::export.export_clients'))
                         ->addClass('export-button')
                         );
        
		parent::init();
	}
}