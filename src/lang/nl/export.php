<?php
return [
    'export_clients' => 'Klanten exporteren',
    'date_from' => 'Vanaf datum',
    'date_till' => 'Tot datum',
    
    'export' => 'Exporteren',
    'download_export' => 'Exportbestand downloaden',
];